// Locales
import { en } from 'vuetify/locale';

// Styles
import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'

// Composables
import { createVuetify } from 'vuetify'

// Types
import type { VuetifyOptions } from 'vuetify'

let vuetifyConfig: VuetifyOptions = {
  locale: {
    locale: 'en',
    fallback: 'en',
    messages: { en },
  },
  theme: {
    themes: {
      light: {
        colors: {
          primary: '#1867C0',
          secondary: '#5CBBF6',
        },
      },
    },
  },
}

export default createVuetify(vuetifyConfig);
